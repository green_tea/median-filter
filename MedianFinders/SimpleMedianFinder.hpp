#ifndef MEDIAN_FILTER_SIMPLEMEDIANFINDER_H
#define MEDIAN_FILTER_SIMPLEMEDIANFINDER_H

#include "MedianFinder.h"

template <typename T, typename S>
class SimpleMedianFinder : MedianFinder<T> {
protected:
    S sorter;

public:
    template<typename T, typename S>
    T MedianFilter<T, S>::operator()(T *data, std::size_t start, std::size_t finish) {
        T* new_data = new T*[finish - start + 1];
        std::copy(data + start, data + finish, new_data);
        sorter(new_data);

        std::size_t size = (finish - start + 1);
        T response = new_data[size / 2];

        delete new_data;
        return response;
    }
};


#endif //MEDIAN_FILTER_SIMPLEMEDIANFINDER_H
