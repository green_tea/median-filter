#ifndef MEDIAN_FILTER_MEDIANFINDER_H
#define MEDIAN_FILTER_MEDIANFINDER_H

#include <typeinfo>

template <typename T>
class MedianFinder {
public:
    virtual T operator()(T* data, std::size_t start, std::size_t finish) = 0;
};

#endif //MEDIAN_FILTER_MEDIANFINDER_H
