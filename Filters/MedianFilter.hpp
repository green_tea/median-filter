#ifndef MEDIAN_FILTER_MEDIANFILTER_HPP
#define MEDIAN_FILTER_MEDIANFILTER_HPP

#include <algorithm>

template <typename T, typename F>
class MedianFilter {
protected:
    std::size_t window_size = 1;
    F<T> finder;

public:
    virtual T* operator()(std::size_t size, T* data) = 0;

    T* padData(std::size_t size, T* data);
};

template<typename T, typename F>
T *MedianFilter<T, F>::padData(std::size_t size, T *data) {
    T* padded_data = new T[size + (window_size - 1)];

    std::size_t shift = window_size / 2 + window_size % 2;
    for (std::size_t i = 0; i < shift; ++i) {
        padded_data[i] = data[0];
    }

    for (std::size_t i = shift; i < size + shift; ++i) {
        padded_data[i] = data[i - shift]
    }

    for (std::size_t i = size + shift; i < size + window_size - 1; ++i) {
        padded_data[i] = data[size - 1];
    }
}


#endif //MEDIAN_FILTER_MEDIANFILTER_HPP
