#ifndef MEDIAN_FILTER_SIMPLEMEDIANFILTER_HPP
#define MEDIAN_FILTER_SIMPLEMEDIANFILTER_HPP

#include "MedianFilter.hpp"

template <typename T, typename F>
class SimpleMedianFilter : MedianFilter<T, F> {
    T* operator()(std::size_t size, T* data) override;
};

template<typename T, typename F>
T *SimpleMedianFilter<T, F>::operator()(std::size_t size, T* data) {
    T* padded_data = this->padData(size, data);

    T* result = new T[size];

    for (int start = 0, finish = this->window_size; finish++ < size + (this->window_size - 1) / 2; ++start) {
        result[start] = this->finder(padded_data, start, finish);
    }

    delete padded_data;
    return result;
}


#endif //MEDIAN_FILTER_SIMPLEMEDIANFILTER_HPP
