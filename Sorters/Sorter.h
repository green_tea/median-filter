#ifndef MEDIAN_FILTER_SORTER_H
#define MEDIAN_FILTER_SORTER_H

#include <typeinfo>

template <typename T>
class Sorter {
public:
    virtual void operator()(T* data, std::size_t start, std::size_t finish) const = 0;
};

#endif //MEDIAN_FILTER_SORTER_H
