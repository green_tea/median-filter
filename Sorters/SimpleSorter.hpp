#ifndef MEDIAN_FILTER_SIMPLESORTER_HPP
#define MEDIAN_FILTER_SIMPLESORTER_HPP

#include "Sorter.h"

#include <algorithm>

template <typename T>
class SimpleSorter : Sorter<T> {
public:
    void operator()(T* data, std::size_t start, std::size_t finish) const override;
};

template<typename T>
void SimpleSorter<T>::operator()(T *data, std::size_t start, std::size_t finish) const {
    std::sort(data, data + start, data + finish);
}


#endif //MEDIAN_FILTER_SIMPLESORTER_HPP
